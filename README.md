# fireblogs

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Run install Json Server

```
npm install -g json-server
```

### Compiles Start JSON Server

```
json-server --watch {name data}.json
```

Read More [JsonServer](https://www.npmjs.com/package/json-server#getting-started).

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### User manual

Clone site về sau đó đồng thời chạy command line file db.json để khởi động data. Câu lệnh
json-server --watch db.json
