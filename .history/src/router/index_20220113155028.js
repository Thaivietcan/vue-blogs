import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Blogs from "../views/Blogs.vue";
import ListUser from "../views/ListUser.vue";
import CreatePost from "../views/CreatePost.vue";
import Login from "../views/Login.vue";
import Register from "../views/Register.vue";
import ForgetPassword from "../views/ForgetPassword.vue";
import Profile from "../views/Profile.vue";
import Admin from "../views/Admin.vue";
import PostDetail from "../views/PostDetail.vue";
import UpdateUser from "../views/UpdateUser.vue";
import UpdatePost from "../views/UpdatePost.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/blogs",
    name: "Blogs",
    component: Blogs,
  },
  {
    path: "/post/:id",
    name: "PostDetail",
    component: PostDetail,
  },
  {
    path: "/list-user",
    name: "ListUser",
    component: ListUser,
  },
  {
    path: "/create-post",
    name: "CreatePost",
    component: CreatePost,
  },
  {
    path: "/update-post/:id",
    name: "UpdatePost",
    component: UpdatePost,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
  },
  {
    path: "/forget-password",
    name: "ForgetPassword",
    component: ForgetPassword,
  },
  {
    path: "/update/:id",
    name: "UpdateUser",
    component: UpdateUser,
  },
  {
    path: "/profile",
    name: "Profile",
    component: Profile,
  },
  {
    path: "/admin",
    name: "Admin",
    component: Admin,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
