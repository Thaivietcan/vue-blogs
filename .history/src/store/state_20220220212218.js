export default {
  /*
   *  Noi luu tru cac trang thai cua ung dung
   */

  blogCard: [],

  welcomeScreen: {
    title: "Welcome !",
    blogPost:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen boo",
    welcomeScreen: true,
    photo: "coding",
  },

  sampleBlogs: [
    {
      title: "What is Lorem Ipsum?",
      content:
        "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      image: "beautiful-stories",
    },
    {
      title: "Why do we use it?",
      content:
        "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      image: "designed-for-everyone",
    },
  ],

  editPost: null,
};
