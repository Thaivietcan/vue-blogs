export default {
  /**
   * la nhung ham de thay doi state
   * @param {*} state
   * @param {*} blogCard
   */

  // Fetch All blog
  setBlogs(state, blogCard) {
    state.blogCard = blogCard;
  },

  // Remove blog by id
  removeBlog(state, id) {
    state.blogCard = state.blogCard.filter((blog) => blog.id !== id);
  },

  // Toggle Edit Post
  toggleEditPost(state, payload) {
    state.editPost = payload;
    console.log(state.editPost);
  },

  // Fetch Users
  setUsers(state, users) {
    state.users = users;
  },

  // Remove user by id
  removeUser(state, id) {
    state.users = state.users.filter((user) => user.id !== id);
  },
};
