import axios from "axios";
import state from "./state";
import getters from "./getters";
import actions from "./actions";
import mutations from "./mutations";

const storeConfigs = {
  state,
  getters,
  actions,

  mutations: {
    setBlogs(state, blogCard) {
      state.blogCard = blogCard;
    },
    // newBlog: (state, data) => state.blogCard.push(data),
    removeBlog(state, id) {
      state.blogCard = state.blogCard.filter((blog) => blog.id !== id);
    },
  },
};

export default storeConfigs;
