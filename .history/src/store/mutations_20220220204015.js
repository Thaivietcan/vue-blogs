export default {
  /*
   * la nhung ham de thay doi state
   */

  // Fetch All blog
  setBlogs(state, blogCard) {
    state.blogCard = blogCard;
  },

  removeBlog(state, id) {
    state.blogCard = state.blogCard.filter((blog) => blog.id !== id);
  },
};
