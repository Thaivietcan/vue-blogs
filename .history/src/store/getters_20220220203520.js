export default {
  /**
   * la nhung ham de lay ra state
   * @param {*} state
   * @returns
   */
  allBlogs: (state) => state.blogCard, // get all blogs
};
