import axios from "axios";
export default {
  // Function get blog
  async fetchBlogs({ commit }) {
    const response = await axios.get("http://localhost:3000/posts");
    commit("setBlogs", response.data);
  },

  // Function delete
  async deleteBlog({ commit }, id) {
    await axios.delete(`http://localhost:3000/posts/${id}`);
    commit("removeBlog", id);
  },

  // Post Home
  async allBlogHome({ commit }) {
    const response = await axios.get("http://localhost:3000/posts?_limit=4");
    commit("setBlogs", response.data);
  },

  // Filter Limit Blog
  async limitBlog({ commit }, e) {
    const limit = parseInt(
      e.target.options[e.target.options.selectedIndex].innerText
    );
    const response = await axios.get(
      `http://localhost:3000/posts?_limit=${limit}`
    );

    commit("setBlogs", response.data);
  },

  async fetchUsers({ commit }) {
    const response = await axios.get("http://localhost:3000/users");
    commit("setUsers", response.data);
  },
};
