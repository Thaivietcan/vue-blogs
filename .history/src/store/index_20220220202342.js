import axios from "axios";
const storeConfigs = {
  state: {
    blogCard: [],
  },

  getters: {
    allBlogs: (state) => state.blogCard, // get all blogs
  },
  actions: {
    // Function get blog
    async fetchBlogs({ commit }) {
      const response = await axios.get("http://localhost:3000/posts");
      // this.state.blogCard = response.data;
      commit("setBlogs", response.data);
    },

    // Function delete
    async deleteBlog({ commit }, id) {
      await axios.delete(`http://localhost:3000/posts/${id}`);
      commit("removeBlog", id);
    },

    // Filter Limit Blog
    async limitBlog({ commit }, e) {
      const limit = parseInt(
        e.target.options[e.target.options.selectedIndex].innerText
      );
      const response = await axios.get(
        `http://localhost:3000/posts?_limit=${limit}`
      );

      commit("setBlogs", response.data);
    },
  },

  mutations: {
    setBlogs(state, blogCard) {
      state.blogCard = blogCard;
    },
    // newBlog: (state, data) => state.blogCard.push(data),
    removeBlog(state, id) {
      state.blogCard = state.blogCard.filter((blog) => blog.id !== id);
    },
  },
};

export default storeConfigs;
