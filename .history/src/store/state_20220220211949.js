export default {
  /*
   *  Noi luu tru cac trang thai cua ung dung
   */

  blogCard: [],
  sampleBlogs: [
    {
      title: "What is Lorem Ipsum?",
      content:
        "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      image: "beautiful-stories",
    },
    {
      title: "Why do we use it?",
      content:
        "when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      image: "designed-for-everyone",
    },
  ],

  editPost: null,
};
