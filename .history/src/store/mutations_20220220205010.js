export default {
  /*
   * la nhung ham de thay doi state
   */

  // Fetch All blog
  setBlogs(state, blogCard) {
    state.blogCard = blogCard;
  },

  // Remove blog by id
  removeBlog(state, id) {
    state.blogCard = state.blogCard.filter((blog) => blog.id !== id);
  },

  // Toggle Edit Post
  toggleEditPost(state, payload) {
    state.editPost = payload;
    console.log(state.editPost);
  },
};
