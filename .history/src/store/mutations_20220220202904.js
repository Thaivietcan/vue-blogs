export default {
  setBlogs(state, blogCard) {
    state.blogCard = blogCard;
  },
  // newBlog: (state, data) => state.blogCard.push(data),
  removeBlog(state, id) {
    state.blogCard = state.blogCard.filter((blog) => blog.id !== id);
  },
};
