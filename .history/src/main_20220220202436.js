import Vue from "vue";
import Vuex from "vuex";
import App from "./App.vue";
import router from "./router";
import storeConfigs from "./store";
import Vue2Editor from "vue2-editor";

Vue.use(Vuex, Vue2Editor);

const store = new Vuex.Store(storeConfigs);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
