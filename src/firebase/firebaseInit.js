import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyArrx0-9hFDZKeVo5NoQF7A5VhhW-Rnw-k",
  authDomain: "fireblogs-a0b84.firebaseapp.com",
  projectId: "fireblogs-a0b84",
  storageBucket: "fireblogs-a0b84.appspot.com",
  messagingSenderId: "59761434248",
  appId: "1:59761434248:web:1b6948771bf5d23a06493d",
};

const firebaseApp = initializeApp(firebaseConfig);
const db = getFirestore(firebaseApp);

export { db };
